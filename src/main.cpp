#include <opencv2/opencv.hpp>

using namespace cv;

#define REWIND_AMOUNT 150

String type2str(int type) {
    String r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar chans = 1 + (type >> CV_CN_SHIFT);

    switch ( depth ) {
        case CV_8U:  r = "8U"; break;
        case CV_8S:  r = "8S"; break;
        case CV_16U: r = "16U"; break;
        case CV_16S: r = "16S"; break;
        case CV_32S: r = "32S"; break;
        case CV_32F: r = "32F"; break;
        case CV_64F: r = "64F"; break;
        default:     r = "User"; break;
    }

    r += "C";
    r += (chans+'0');

    return r;
}

void concatImages() {
    Size imSize(640, 384);
    Mat image1 = imread( "./keanu/keanu_1.jpeg", 1 );
    Mat image2 = imread( "./keanu/keanu_2.jpeg", 1 );
    Mat image3 = imread( "./keanu/keanu_3.jpeg", 1 );
    Mat image4 = imread( "./keanu/keanu_4.jpeg", 1 );
    Mat resultFrame(Size(1280, 768), image1.type());

    resize(image1, image1, imSize);
    resize(image2, image2, imSize);
    resize(image3, image3, imSize);
    resize(image4, image4, imSize);

    image1.copyTo(resultFrame(Rect(Point(0, 0), Size(image1.cols, image1.rows))));
    image2.copyTo(resultFrame(Rect(Point(1 * imSize.width, 0 * imSize.height), Size(image2.cols, image2.rows))));
    image3.copyTo(resultFrame(Rect(Point(0 * imSize.width, 1 * imSize.height), Size(image3.cols, image3.rows))));
    image4.copyTo(resultFrame(Rect(Point(1 * imSize.width, 1 * imSize.height), Size(image4.cols, image4.rows))));


    imshow("Display Image", resultFrame);

    waitKey(0);
}

void concatVideos() {
    VideoCapture video1("./video/Green-kitchen01.dav");
    VideoCapture video2("./video/Green-Lift.dav");
    VideoCapture video3("./video/Green-Reception.dav");
    VideoCapture video4("./video/Green-Stairs2.dav");

    Mat frame1;
    Mat frame2;
    Mat frame3;
    Mat frame4;
    Mat resultFrame(Size(1280, 768), CV_8UC3);
    Size imSize(resultFrame.cols / 2, resultFrame.rows / 2);

    video1.read(frame1);
    video2.read(frame2);
    video3.read(frame3);
    video4.read(frame4);

    resize(frame1, frame1, imSize);
    resize(frame2, frame2, imSize);
    resize(frame3, frame3, imSize);
    resize(frame4, frame4, imSize);

    Rect roi1(Point(0, 0), Size(frame1.cols, frame1.rows));
    Rect roi2(Point(1 * imSize.width, 0 * imSize.height), Size(frame2.cols, frame2.rows));
    Rect roi3(Point(0 * imSize.width, 1 * imSize.height), Size(frame3.cols, frame3.rows));
    Rect roi4(Point(1 * imSize.width, 1 * imSize.height), Size(frame4.cols, frame4.rows));

    int key = 0;

    while(key != 27) {
        video1.read(frame1);
        video2.read(frame2);
        video3.read(frame3);
        video4.read(frame4);

        resize(frame1, frame1, imSize);
        resize(frame2, frame2, imSize);
        resize(frame3, frame3, imSize);
        resize(frame4, frame4, imSize);

        frame1.copyTo(resultFrame(roi1));
        frame2.copyTo(resultFrame(roi2));
        frame3.copyTo(resultFrame(roi3));
        frame4.copyTo(resultFrame(roi4));

        namedWindow("Display Image", WINDOW_AUTOSIZE);

        imshow("Display Image", resultFrame);

        // 0 wait untill pressed, > 0 ms to wait
        key = waitKey(1) & 0xFF;

        switch (key) {
            case 2:
                video1.set(CAP_PROP_POS_MSEC, video1.get(CAP_PROP_POS_MSEC) + REWIND_AMOUNT);
                video2.set(CAP_PROP_POS_MSEC, video2.get(CAP_PROP_POS_MSEC) + REWIND_AMOUNT);
                video3.set(CAP_PROP_POS_MSEC, video3.get(CAP_PROP_POS_MSEC) + REWIND_AMOUNT);
                video4.set(CAP_PROP_POS_MSEC, video4.get(CAP_PROP_POS_MSEC) + REWIND_AMOUNT);
                break;
            case 3:
                video1.set(CAP_PROP_POS_MSEC, video1.get(CAP_PROP_POS_MSEC) - REWIND_AMOUNT);
                video2.set(CAP_PROP_POS_MSEC, video2.get(CAP_PROP_POS_MSEC) - REWIND_AMOUNT);
                video3.set(CAP_PROP_POS_MSEC, video3.get(CAP_PROP_POS_MSEC) - REWIND_AMOUNT);
                video4.set(CAP_PROP_POS_MSEC, video4.get(CAP_PROP_POS_MSEC) - REWIND_AMOUNT);
                break;

            default:
                break;
        }
    }

    video1.release();
    video2.release();
    video3.release();
    video4.release();

    destroyAllWindows();
}

int main(int argc, char** argv )
{
    namedWindow("Display Image", WINDOW_AUTOSIZE);
    concatVideos();
    return 0;
}